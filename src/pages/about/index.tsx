import { NextPage } from "next";
import styles from './styles.module.scss';
import Component from "components/component";

const About: NextPage = () => {
    return <div className={styles.root}><Component /></div>
}

export default About;