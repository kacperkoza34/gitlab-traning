import Head from "next/head";
import Image from "next/image";
import { useState } from "react";
import styles from "./styles.module.scss";

export default function Home() {
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);
  const [result, setResult] = useState(0);
  const add = () => {
    setResult(parseInt(num1.toString()) + parseInt(num2.toString()));
  };
  const subtract = () => {
    setResult(parseInt(num1.toString()) - parseInt(num2.toString()));
  };
  const multiply = () => {
    setResult(parseInt(num1.toString()) * parseInt(num2.toString()));
  };
  const divide = () => {
    setResult(parseInt(num1.toString()) / parseInt(num2.toString()));
  };
  return (
    <div className={styles.root} >
      <div className={styles.container}>
        <Head>
          <title>Create Next App</title>
          <meta name="description" content="Generated by create next app" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <div className={styles.result} data-testid="result">
          {result}
        </div>
        <input
          type="number"
          className={styles.input}
          data-testid="num1"
          value={num1}
          onChange={(e) => setNum1(Number(e.target.value))}
        />
        <input
          type="number"
          className={styles.input}
          data-testid="num2"
          value={num2}
          onChange={(e) => setNum2(Number(e.target.value))}
        />
        <button onClick={add} className={styles.button} data-testid="add">
          Add
        </button>
        <button
          onClick={subtract}
          className={styles.button}
          data-testid="subtract"
        >
          Subtract
        </button>
        <button
          onClick={multiply}
          className={styles.button}
          data-testid="multiply"
        >
          Multiply
        </button>
        <button onClick={divide} className={styles.button} data-testid="divide">
          Divide
        </button>
      </div>
    </div>
  );
}